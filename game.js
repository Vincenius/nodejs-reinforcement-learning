let board = [];

module.exports = {
    startGame: function() {
        board = [[0, 0, 0], 
                [0, 0, 0], 
                [0, 0, 0]]; // empty board
    },
    getBoard: function() {
        return board;
    },
    printBoard: function() {
        for (let x = 0; x < 3; x++) {
            console.log(board[x]);
        }
    },
    getPossibleActions: function() {
        let possibleActions = [];
        for (let x = 0; x < 3; x++) {
            for (let y = 0; y < 3; y++) {
                if (board[x][y] === 0) {
                    possibleActions.push([x,y]);
                }
            }
        }

        return possibleActions;
    },
    makeAction: function(x, y, player) {
        let winner = 0;
        if (board[x][y] === 0) {
            board[x][y] = player;
            winner = this.checkWinner(board);
        }

        return winner;
    },
    checkWinner: function(board) {
        for (let x = 0; x < 3; x++) {
            let sameInARow = 0;
            let prevRowItem = 0;

            let sameInAColumn = 0;
            let prevColumnItem = 0;

            for (let y = 0; y < 3; y++) {
                // check row
                if (board[x][y] !== 0) { // is not empty
                    if (y === 0) { // first item in row
                        prevRowItem = board[x][y];
                        sameInARow++;
                    } else if (board[x][y] === prevRowItem) {
                        sameInARow++;
                    }
                }

                // check column (switch x & y)
                if (board[y][x] !== 0) { // is not empty
                    if (y === 0) { // first item in row
                        prevColumnItem = board[y][x];
                        sameInAColumn++;
                    } else if (board[y][x] === prevColumnItem) {
                        sameInAColumn++;
                    }
                }
            }

            if (sameInARow === 3) {
                return board[x][0];
            }
            if (sameInAColumn === 3) {
                return board[0][x];
            }
        }

        // check horizontal
        if (board[0][0] !== 0 && board[0][0] === board[1][1] && board[0][0] === board[2][2]) {
            return board[0][0];
        }
        if (board[0][2] !== 0 && board[0][2] === board[1][1] && board[0][2] === board[2][0]) {
            return board[0][2];
        }

        return 0;
    }
}