const fs = require('fs');
const game = require('./game');
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const baseReward = 100;
const discountFactor = 0.4;
const trainingIterations = 50000;
const explorationRate = 0.00001;
const minExplorationRate = 0.05;

let qTable = {}

function playBotVsBot(explore) {
    game.startGame();

    // init game variables
    let possibleActions = game.getPossibleActions();
    let activePlayer = 1;
    let agent1 = []; // to store the steps made by bots
    let agent2 = [];
    let winner = 0;

	while ((possibleActions.length > 0 && winner === 0)) {
        let board = JSON.parse(JSON.stringify( game.getBoard() )); // make a copy of the board
        let randomAction = Math.random(); // value between 0 and 1
        let action;
        
        if (randomAction < explore) { 
            // get random action
            let actionIndex = getRandomInt(0, (possibleActions.length - 1));
            action = possibleActions[actionIndex];
        } else {
            // try to find best action - fallback is random action
            action = findBestAction(board).action;
            if (action === undefined) {
                let actionIndex = getRandomInt(0, (possibleActions.length - 1));
                action = possibleActions[actionIndex];
            }
        }

        winner = game.makeAction(action[0],action[1], activePlayer);

        // save step made by agent
        let step = {
            state: board,
            action,
            reward: 0
        }
        if (activePlayer === 1) {
            agent1.push(step);
        } else {
            agent2.push(step);
        }

        possibleActions = game.getPossibleActions(); // get new possible action
       	activePlayer = activePlayer * -1; // next player
    }

    if (winner === 1) {
        agent1 = addRewardToSteps(agent1, 1);
        agent2 = addRewardToSteps(agent2, -1);
    } else if (winner === -1) {
        agent1 = addRewardToSteps(agent1, -1);
        agent2 = addRewardToSteps(agent2, 1);
    }

    return {agent1, agent2};
}

function addRewardToSteps(steps, result) {
    // result is either 1 or -1 depending if agent won or lost
    let reward = baseReward * result; // get positive or negative reward

    const numberOfSteps = steps.length;
    for (let i = (numberOfSteps - 1); i >= 0; i--) {
        steps[i].reward = reward;
        reward = reward * discountFactor; // reduce reward for previous step
    }

    return steps;
}

function addLearning(steps) {
    for (step of steps) {
        let state = step.state.toString();
        let action = step.action.toString();

        // create state if not exist
        if (qTable[state] === undefined) {
            qTable[state] = { };
        }

        if (qTable[state][action] !== undefined) {
            // update reward if action exists
            qTable[state][action].reward += step.reward;
        } else {
            // add action if not exists
            qTable[state][action] = { reward: step.reward, action: step.action };
        }
    }
}

function findBestAction(state) {
    bestAction = {}
    if (qTable[state] !== undefined) {
        for (let actionElement in qTable[state]) {
            if (bestAction.reward === undefined || qTable[state][actionElement].reward > bestAction.reward) {
                bestAction = {
                    action: qTable[state][actionElement].action,
                    reward: qTable[state][actionElement].reward
                }
            }
        }
    }

    return bestAction;
}

function startTraining() {
    let explore = 1;
    console.log('started training...');

    for (let i = 0; i < trainingIterations; i++) {
        // start training game
        let agents = playBotVsBot(explore);

        addLearning(agents.agent1);
        addLearning(agents.agent2);

        // reduce explore rate
        if (explore > minExplorationRate) {
            explore -= explorationRate;
        }
    }
    console.log('training done!');
}

function startGameAgainstPlayer() {
    game.startGame();

    console.log('enter x,y');
    
    rl.on('line', (input) => {
        try {
            let x = parseInt(input[0]);
            let y = parseInt(input[2]);
            let possibleActions = game.getPossibleActions();
            let action = [x, y];
            let actionAvailable = false;

            // check if user action is possible
            for (let possibleAction of possibleActions) {
                if (possibleAction.toString() === action.toString()) {
                    actionAvailable = true;
                }
            }

            if (!actionAvailable) {
                console.log('wrong input or already taken');
            } else {
                let winner = game.makeAction(x,y, 1);
                let board = game.getBoard();
                let gameEnd = checkWinner(winner, game);

                if (!gameEnd) {
                    let botAction = findBestAction(board).action;
                    winner = game.makeAction(botAction[0],botAction[1], -1);

                    gameEnd = checkWinner(winner, game);
                }

                if (gameEnd) {
                    game.printBoard();
                    console.log('');
                    game.startGame();
                }

                game.printBoard();
            }
        } catch (err) {
            console.log(err);
        }
    });
}

// helper function to log winner and return if game has ended
function checkWinner(winner, game) {
    if (winner === -1) {
        console.log('BOT WON');
        return true;
    } else if (winner === 1) {
        console.log('YOU WON');
        return true;
    } else if (game.getPossibleActions().length === 0) {
        console.log('DRAW')
        return true;
    }

    return false;
}

function getRandomInt(min, max) {
   return Math.floor(Math.random() * (max - min + 1)) + min;
}

startTraining();
startGameAgainstPlayer();